const express = require('express');
const morgan = require('morgan');
const compress = require('compression');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const helmet = require('helmet');

module.exports = () => {
	const app = express();
	
	app.set('trust proxy', true);
	
	app.use(helmet());
	if (process.env.NODE_ENV === 'development') {
		app.use(morgan('dev'));
	} else if (process.env.NODE_ENV === 'production') {
		app.use(compress());
	}
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json());
	app.use(methodOverride());
	
	// Routings
	const index = require('../routes/index.routes');
	const kakao = require('../routes/kakao-plusfriend.routes');
	const apiai = require('../routes/apiai.routes');
	const speech = require('../routes/speech.routes');
	app.use('/', index);
	app.use('/api/kakao', kakao);
	app.use('/api/apiai', apiai);
	app.use('/api/speech', speech);
	
	return app;
};
