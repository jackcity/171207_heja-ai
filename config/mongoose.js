const config = require('./config');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

module.exports = () => {
	const db = mongoose.connect(config.DB_URI);
	
	require('../models/user.model');
	
	return db;
};
