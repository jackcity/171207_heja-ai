exports.showJSON = (req, res) => {
	if (req.response) {
		let responseJSON = req.response;
		
		console.log('req.response : ', responseJSON);
	} else if (req.errors) {
		let errorJSON = req.errors;
		
		console.log('req.error : ', JSON.stringify(errorJSON));
	}
};
