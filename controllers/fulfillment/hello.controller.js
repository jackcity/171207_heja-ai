exports.hello = (req, res) => {
	let header = { 'Content-Type': 'application/json' };
	let response = 'This is a sample response from your webhook!';
	
	res.set(header)
		.send(JSON.stringify({
			speech: response,
			displayText: response
		}));
};
