const request = require('request');
const config = require('../config/config');
const naverClientID = config.NAVER_API_CLIENT_ID;
const naverClientSecret = config.NAVER_API_CLIENT_SECRET;
const baseUrl = 'https://openapi.naver.com';

exports.searchNews = (req, res, next) => {
	let path = '/v1/search/news.json';
	let options = {
		uri: baseUrl + path,
		headers: {
			'X-Naver-Client-Id': naverClientID,
			'X-Naver-Client-Secret': naverClientSecret
		},
		qs: {
			query: req.keyword,
			display: 1,
			start: 1,
			sort: 'sim'
		}
	};
	
	request(options, (err, res, body) => {
		if (err) {
			console.err(err);
		} else {
			req.searchNews = JSON.parse(body);
			
			next();
		}
	});
};
