exports.getPopular = () => {
	const article = {};
	const baseElem = 'tbody td.content div.ranking_top3 li.num1 ';
	const getThumbnail = () => {
		let elem = document.querySelector(baseElem + 'div.thumb img');
		
		if (elem) {
			article.thumb = elem.getAttribute('src');
		} else {
			article.thumb = '';
		}
	};
	const getTitle = () => {
		let elem = document.querySelector(baseElem + 'dt a');
		let naverNews = 'http://news.naver.com';
		let href = elem.getAttribute('href');
		
		article.title = elem.getAttribute('title');
		article.href = naverNews + href;
	};
	const getSummary = () => {
		let elem = document.querySelector(baseElem + 'dd');
		let span1 = document.querySelector(baseElem + 'dd span:nth-child(1)');
		let span2 = document.querySelector(baseElem + 'dd span:nth-child(2)');
		let span3 = document.querySelector(baseElem + 'dd span:nth-child(3)');
		let text = elem.innerText;
		let spans = [
			span1.innerText,
			span2.innerText,
			span3.innerText
		];
		
		for (let i = 0; i < spans.length; i++) {
			text = text.replace(spans[i], '');
		}
		
		article.summary = text;
	};
	const getMedia = () => {
		let elem = document.querySelector(baseElem + 'dd span:nth-child(1)');

		article.media = elem.innerText;
	};
	
	getThumbnail();
	getTitle();
	getSummary();
	getMedia();
	
	return article;
};

exports.getEntertain = () => {
	const article = {};
	const baseElem = 'ul#ranking_list li:nth-child(1) ';
	const getThumbnail = () => {
		let elem = document.querySelector(baseElem + 'a.thumb_area img');
		
		if (elem) {
			article.thumb = elem.getAttribute('src');
		} else {
			article.thumb = '';
		}
	};
	const getTitle = () => {
		let elem = document.querySelector(baseElem + 'div.tit_area a.tit');
		let naverEntertain = 'http://entertain.naver.com';
		
		article.title = elem.innerText;
		article.href = naverEntertain + elem.getAttribute('href');
	};
	const getSummary = () => {
		let elem = document.querySelector(baseElem + 'div.tit_area p.summary');
		
		article.summary = elem.innerText;
	};
	const getMedia = () => {
		article.media = '';
	};
	
	getThumbnail();
	getTitle();
	getSummary();
	getMedia();
	
	return article;
};

exports.getSports = () => {
	const article = {};
	const baseElem = 'div#_newsList ul li:nth-child(1) ';
	const getThumbnail = () => {
		let elem = document.querySelector(baseElem + 'a.thmb img');
		
		if (elem) {
			article.thumb = elem.getAttribute('src');
		} else {
			article.thumb = '';
		}
	};
	const getTitle = () => {
		let hrefElem = document.querySelector(baseElem + 'div.text a.title');
		let titleElem = document.querySelector(baseElem + 'div.text a.title span');
		let naverSports = 'http://sports.news.naver.com';
		let href = hrefElem.getAttribute('href');
		
		article.title = titleElem.innerText;
		article.href = naverSports + href;
	};
	const getSummary = () => {
		let elem = document.querySelector(baseElem + 'div.text p.desc');
		
		article.summary = elem.innerText;
	};
	const getMedia = () => {
		let elem = document.querySelector(baseElem + 'div.text div.source span.press');
		
		article.media = elem.innerText;
	};
	
	getThumbnail();
	getTitle();
	getSummary();
	getMedia();
	
	return article;
};
