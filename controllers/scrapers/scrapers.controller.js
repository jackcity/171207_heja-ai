const Nightmare = require('nightmare');
const { URL, URLSearchParams } = require('url');
const naverNewsScrp = require('./naver-news.scraper');
const daumNewsScrp = require('./daum-news.scraper');
const naverApiCtrl = require('../naver-api.controller');

const addDigits = (num, digits) => {
	let zero = '';
	num = num.toString();
	
	if (num.length < digits) {
		for (let i = 0; i < digits - num.length; i++) {
			zero += '0';
		}
	}
	
	return zero + num;
};

const getDate = () => {
	let now = new Date();
	let thisYear = now.getFullYear();
	let thisMonth = now.getMonth() + 1;
	let thisDate = now.getDate();
	
	let m = addDigits(thisMonth, 2);
	let d = addDigits(thisDate, 2);
	
	return thisYear + m + d;
};

const removeSpecialChar = (text) => {
	let specialChar = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
	let emptySpace = / +/g;
	
	if (specialChar.test(text)) {
		let filteredText = text.replace(specialChar, '');
		
		if (emptySpace.test(filteredText)) {
			return filteredText.replace(emptySpace, '');
		}
	} else {
		if (emptySpace.test(text)) {
			return text.replace(emptySpace, '');
		} else {
			return text;
		}
	}
};

const getSection = (keyword) => {
	let isIncluded = (word) => {
		return keyword.indexOf(word) > -1;
	};
	
	if (isIncluded('정치')) return 'politics';
	else if (isIncluded('경제')) return 'economy';
	else if (isIncluded('사회')) return 'society';
	else if (isIncluded('생활') || isIncluded('문화')) return 'life';
	else if (isIncluded('국제') || isIncluded('세계')) return 'world';
	else if (isIncluded('아이티') || isIncluded('과학')) return 'it';
	else if (isIncluded('연예')) return 'entertain';
	else if (isIncluded('스포츠')) return 'sports';
	else if (isIncluded('아무') || isIncluded('알아서') || isIncluded('추천') || isIncluded('많이본') || isIncluded('종합')) return 'total';
	else return 'search';
};

const getNaverNewsURL = (sectionId) => {
	let url;
	let params;
	
	switch (sectionId) {
		case '106':
			url = 'http://entertain.naver.com/ranking';
			params = new URLSearchParams({
				rankingType: 'popular_day',
				sectionId: sectionId,
				date: getDate()
			});
			break;
		case '107':
			url = 'http://sports.news.naver.com/general/news/index.nhn';
			params = new URLSearchParams({
				type: 'popular'
			});
			break;
		default:
			url = 'http://news.naver.com/main/ranking/popularDay.nhn';
			params = new URLSearchParams({
				rankingType: 'popular_day',
				sectionId: sectionId,
				date: getDate()
			});
			break;
	}
	
	let newsURL = new URL(url);
	newsURL.search = params.toString();
	
	return newsURL.href;
};

const getDaumNewsURL = (section) => {
	let daumNewsUrl = 'http://media.daum.net';
	let pathPopular = '/ranking/popular';
	let newsURL = new URL(daumNewsUrl + pathPopular);
	
	if (section !== 'total') {
		let params = new URLSearchParams({
			include: section
		});
		newsURL.search = params.toString();
	}
	
	return newsURL.href;
};

exports.scrapeArticles = (req, res, next) => {
	if (req.keyword) {
		const keyword = removeSpecialChar(req.keyword);
		const section = getSection(keyword);
		
		const naverResult = (sectionId) => {
			let nightmare = new Nightmare({ show: false });
			let url = getNaverNewsURL(sectionId);
			
			if (sectionId === '106') {
				try {
					return nightmare
						.goto(url)
						.wait('#ranking_list')
						.evaluate(naverNewsScrp.getEntertain)
						.end();
				} catch (err) {
					console.error(err);
				}
			} else if (sectionId === '107') {
				try {
					return nightmare
						.goto(url)
						.wait('#_newsList')
						.evaluate(naverNewsScrp.getSports)
						.end();
				} catch (err) {
					console.error(err);
				}
			} else {
				try {
					return nightmare
						.goto(url)
						.wait('.ranking_top3')
						.evaluate(naverNewsScrp.getPopular)
						.end();
				} catch (err) {
					console.error(err);
				}
			}
		};
		
		const daumResult = (section) => {
			let nightmare = new Nightmare({ show: false });
			let url = getDaumNewsURL(section);
			
			try {
				return nightmare
					.goto(url)
					.wait('.list_news2')
					.evaluate(daumNewsScrp.getTotal)
					.end();
			} catch (err) {
				console.error(err);
			}
		};
		
		const handleArticle = (sectionId) => {
			let article = naverResult(sectionId);
			
			article.then((result) => {
				console.log('article : ', result);
				req.article = result;
				
				next();
			});
		};
		
		switch (section) {
			case 'total':
				handleArticle('000');
				// let articles = [
				// 	naverResult('000'),
				// 	daumResult(section)
				// ];
				//
				// Promise.all(articles).then((results) => {
				// 	results.map((result) => {
				// 		console.log('result : ', result);
				// 	});
				// 	next();
				// });
				break;
			case 'politics':
				handleArticle('100');
				break;
			case 'economy':
				handleArticle('101');
				break;
			case 'society':
				handleArticle('102');
				break;
			case 'life':
				handleArticle('103');
				break;
			case 'world':
				handleArticle('104');
				break;
			case 'it':
				handleArticle('105');
				break;
			case 'entertain':
				handleArticle('106');
				// naverResult('106');
				// daumResult(section);
				break;
			case 'sports':
				handleArticle('107');
				// naverResult('107');
				// daumResult(section);
				break;
			case 'search':
				naverApiCtrl.searchNews(req, res, next);
				break;
		}
	} else {
		next();
	}
};
