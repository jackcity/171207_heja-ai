exports.getTotal = () => {
	const article = {};
	const baseElem = 'div#mArticle div.rank_news ul.list_news2 li:nth-child(1) ';
	const getThumbnail = () => {
		let elem = document.querySelector(baseElem + 'a.link_thumb img');
		
		if (elem) {
			article.thumb = elem.getAttribute('src');
		} else {
			article.thumb = '';
		}
	};
	const getTitle = () => {
		let elem1 = document.querySelector(baseElem + 'div.cont_thumb strong a');
		let elem2 = document.querySelector(baseElem + 'div.cont_thumb strong span');
		
		article.title = elem1.innerText;
		article.href = elem1.getAttribute('href');
		article.media = elem2.innerText;
	};
	const getSummary = () => {
		let elem = document.querySelector(baseElem + 'div.cont_thumb div.desc_thumb span');
		
		article.summary = elem.innerText;
	};
	
	getThumbnail();
	getTitle();
	getSummary();
	
	return article;
};
