const config = require('../config/config');
const apiai = require('apiai');
const apiaiClient = apiai(config.APIAI_CLIENT_ACCESS_TOKEN);

exports.requestText = (req, res, next) => {
	let userMessage = req.userMessage;
	let options = { sessionId: userMessage.user_key };
	let request = apiaiClient.textRequest(userMessage.content, options);

	request.on('response', (response) => {
		handleResponse(req, userMessage.user_key, response);
		
		next();
	});
	
	request.on('error', (err) => {
		handleError(err, req);
		
		next();
	});
	
	request.end();
};

function handleResponse(req, user, response) {
	let responseText = response.result.fulfillment.speech;
	let messages = response.result.fulfillment.messages;
	let action = response.result.action;
	let contexts = response.result.contexts;
	let parameters = response.result.parameters;
	
	req.response = response;
	
	if (action.length && action.length > 0) {
		req.keyword = handleAction(user, action, responseText, contexts, parameters);
	}
}

function handleAction(user, action, responseText, contexts, parameters) {
	switch (action) {
		case 'news_start':
			if (parameters.news_keyword.length > 0) {
				console.log('PARAMETER >>>>> ', parameters.news_keyword);
				
				return parameters.news_keyword;
			}
			break;
	}
}

function handleError(err, req) {
	req.errors = { dialogflow : err };
	
	console.err(`Error from Dialogflow : ${err}`);
}
