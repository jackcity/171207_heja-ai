exports.setKeyboard = (req, res) => {
	let header = { 'Content-Type': 'application/json' };
	// let keyboard = { type: 'text' };
	let keyboard = {
		type: 'buttons',
		buttons: ['시작하기']
	};
	
	res.set(header).json(keyboard);
};

exports.addFriend = (req, res) => {
	let header = { 'Content-Type': 'application/json' };
	let user_key = req.body.user_key;
	
	res.set(header).json({ success: true });
	
	console.log(`${ user_key } added Heja as a friend.`);
};

exports.blockFriend = (req, res) => {
	let header = { 'Content-Type': 'application/json' };
	let user_key = req.params.user_key;
	
	res.set(header).json({ success: true });
	
	console.log(`${ user_key } blocked Heja from friends.`);
};

exports.exitChatRoom = (req, res) => {
	let header = { 'Content-Type': 'application/json' };
	let user_key = req.params.user_key;
	
	res.set(header).json({ success: true });
	
	console.log(`${ user_key } left chat room.`);
};

// Sending message to api.ai
exports.sendMessage = (req, res, next) => {
	let userMessage = {
		user_key: req.body.user_key,
		type: req.body.type,
		content: req.body.content
	};
	
	if (userMessage.content === '시작하기') {
		let header = { 'Content-Type': 'application/json' };
		let responseMessage = {
			message: { text: '안녕하세요, 반갑습니다.\n여러분의 뉴스 검색을 도와줄 혜자입니다.' }
		};
		
		res.set(header).json(responseMessage);
		
		// To bypass the remaining route callbacks
		next('route');
	} else {
		console.log('userMessage : ', userMessage);
		req.userMessage = userMessage;
		
		next();
	}
};

exports.sendResponse = (req, res, next) => {
	let header = { 'Content-Type': 'application/json' };
	
	if (req.response) {
		let responseText = req.response.result.fulfillment.speech;
		let responseMessage = { message: { text: responseText } };
		
		if (req.searchNews) {
			responseMessage.message.message_button = {
				label: req.searchNews.items[0].title,
				url: req.searchNews.items[0].link
			};
		}
		
		if (req.article) {
			let article = req.article;
			
			responseMessage.message = {
				text: article.summary,
				photo: {
					url: article.thumb,
					width: 100,
					height: 100
				},
				message_button: {
					label: article.title,
					url: article.href
				}
			};
		}
		
		res.set(header).json(responseMessage);
	} else if (req.errors) {
		let responseText = '죄송합니다. 오류가 발생했으니 관리자에게 문의바랍니다.';
		
		res.set(header).json({ message : { text : responseText } });
	}
	
	next();
};

/*
exports.sendMessage = (req, res) => {
	let header = { 'Content-Type': 'application/json' };
	let userMessage = {
		user_key: req.body.user_key,
		type: req.body.type,
		content: req.body.content
	};
	let message;
	
	if (userMessage.content === '메뉴 1') {
		message = {
			text: '메뉴1을 선택하셨군요.'
		};
	} else if (userMessage.content === '메뉴 2') {
		message = {
			text: '메뉴2를 선택하셨군요.',
			message_button: {
				label: '라벨형 메세지',
				url: 'https://heja-ai.com'
			}
		};
	} else if (userMessage.content === '메뉴 3') {
		message = {
			text: '메뉴3을 선택하셨군요.',
			photo: {
				url: 'https://tr2.cbsistatic.com/hub/i/r/2017/03/30/4cb08b88-986b-4db5-be59-2549a552a70f/resize/770x/7bb9e6239b1960052ddc33a3c16523d8/screen-shot-2017-03-30-at-11-44-52-am.png',
				width: 720,
				height: 630
			},
			message_button: {
				label: '포토형 메세지',
				url: 'https://heja-ai.com'
			}
		};
	} else {
		message = { text: '반갑습니다. 메뉴를 선택해주세요.' };
	}
	
	let responseMessage = {
		message: message,
		keyboard: {
			type: 'buttons',
			buttons: ['메뉴 1', '메뉴 2', '메뉴 3']
		}
	};
	
	res.set(header).json(responseMessage);
};
*/
