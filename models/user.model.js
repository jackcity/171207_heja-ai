const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	userKey: {
		type: String,
		unique: true,
		required: true
	},
	name: String,
	email: String,
	createdAt: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('User', UserSchema);
