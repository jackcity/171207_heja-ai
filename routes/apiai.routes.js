const router = require('express').Router();
const helloCtrl = require('../controllers/fulfillment/hello.controller');

router.post('/hello', helloCtrl.hello);

module.exports = router;
