const router = require('express').Router();
const indexCtrl = require('../controllers/index.controller');

router.get('/', indexCtrl.render);

module.exports = router;
