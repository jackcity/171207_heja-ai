const router = require('express').Router();
const kakaoCtrl = require('../controllers/kakao-plusfriend.controller');
const apiaiCtrl = require('../controllers/apiai.controller');
const scrapersCtrl = require('../controllers/scrapers/scrapers.controller');
const metadataCtrl = require('../controllers/metadata/metadata.controller');

router.get('/keyboard', kakaoCtrl.setKeyboard);
router.post('/friend', kakaoCtrl.addFriend);
router.delete('/friend/:user_key', kakaoCtrl.blockFriend);
router.delete('/chat_room/:user_key', kakaoCtrl.exitChatRoom);

router.post('/message', kakaoCtrl.sendMessage, apiaiCtrl.requestText, scrapersCtrl.scrapeArticles, kakaoCtrl.sendResponse, metadataCtrl.showJSON);

module.exports = router;
