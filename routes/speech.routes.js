const router = require('express').Router();
const speechCtrl = require('../controllers/speech.controller');
const apiaiCtrl = require('../controllers/apiai.controller');
const scrapersCtrl = require('../controllers/scrapers/scrapers.controller');
const metadataCtrl = require('../controllers/metadata/metadata.controller');
const naverCtrl = require('../controllers/naver-api.controller');

router.post('/', speechCtrl.test);

module.exports = router;
