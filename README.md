# README #

Google Dialogflow(구 api.ai)와 Kakao 플러스 친구를 이용한 뉴스 검색 챗봇입니다.
카카오 플러스친구에서 "혜자"로 검색해 사용해 볼 수 있습니다.
대화관련 Data set은 빈약하여 현재 인사 정도와 뉴스 검색만 가능합니다.
Dialogflow에서 한국어 머신러닝 서비스를 중단하여 직접 문장들을 형태소 분석하고 품사 태깅하여 Entity들을 만들어 사용자 입력을 최소화하려고 시도해 보았습니다.
카카오 플러스친구에서의 제약이 있어 뉴스를 하나 밖에 못보여 줍니다.
그래서 현재 Hybrid App에 Speech Recognition을 붙이고 좀 더 많은 뉴스를 보여줄 수 있도록 개발하고 있습니다.

### 사용 기술 ###
* Back End 
>Proxy Server : NGinx, 
>Server : Node.JS (v.8.x), 
>DB : MongoDB (v.3.x)
 
* Front End 
>Kakao 플러스 친구


### 기능 설명 ###
* 키워드 또는 뉴스의 섹션을 입력하면 각각에 해당하는 네이버 뉴스 중 가장 많이 본 뉴스를 보여줍니다.
