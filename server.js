process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const expressConfiguration = require('./config/express');
const app = expressConfiguration();
const port = process.env.PORT || 8080;

app.listen(port, () => {
	console.log(`App listening on port ${ port }`);
});

module.exports = app;
